stages:
  - build
  - coverity
  - coverity_merge_request
 
variables:
 
cache:

build:
  stage: build
  image: devbox-ubuntu
  variables:
    COVERITY_HOST: "BD-On-Prem-KB.ristow.us"
    COVERITY_STREAM: "$CI_PROJECT_NAME-$CI_COMMIT_BRANCH"
  script:
    - ./config  
    - make clean
    - make --jobs=32 all
    
coverity:
  stage: coverity
  image: coverity
  only:
    - schedules
  variables:
    COVERITY_HOST: "BD-On-Prem-KB.ristow.us"
    COVERITY_STREAM: "$CI_PROJECT_NAME-$CI_COMMIT_BRANCH"
  script:
    - ./config  
    - make clean
    - cov-build --dir idir make --jobs=32 all
    - cov-analyze --dir idir --strip-path $PWD --all --enable-callgraph-metrics 
    - cov-commit-defects --dir idir --host $COVERITY_HOST --ssl --https-port 8448 --on-new-cert trust --stream $COVERITY_STREAM
        --description $CI_PIPELINE_URL --target Linux_x86_64 --version $CI_COMMIT_SHORT_SHA

coverity_merge_request:
  stage: coverity
  # Specify the Docker image that is pre-loaded with your build environment AND the Coverity analysis kit. 
  image: coverity
  # Only run this on merge requests 
  only:
    - merge_requests
    
  variables:
    # Name of my Coverity Connect server     
    COVERITY_HOST: "BD-On-Prem-KB.ristow.us"
    # Name of the stream in Coverity Connect - note how we have made this dynamic to reference the 
    # GitLab CI project name
    COVERITY_STREAM: "$CI_PROJECT_NAME-$CI_MERGE_REQUEST_TARGET_BRANCH_NAME"
    # You may also use an authentication key instead of a hard-coded password
  script:
    # Determine the changed files
    - echo $CI_MERGE_REQUEST_PROJECT_URL +$CI_MERGE_REQUEST_TARGET_BRANCH_NAME
    - git fetch --verbose $CI_MERGE_REQUEST_PROJECT_URL +$CI_MERGE_REQUEST_TARGET_BRANCH_NAME:target_branch
    - git tag -f fork_point $(git merge-base target_branch $CI_COMMIT_SHA)
    # Filter out only source code files to pass into Coverity
    - export CHANGED_FILES="$(git diff --name-only fork_point $CI_COMMIT_SHA | egrep '[.c|.h|.cpp|.hpp|.cs|.java|.js|.py|.php|.rb]$')"
    - ./config
    - make clean
    # First run the build capture - in this case we are running a full build capture, not re-using an intermediate directory
    - >
        cov-run-desktop
        --dir idir
        --host $COVERITY_HOST
        --port 8448
        --ssl
        --on-new-cert trust
        --user ristow
        --stream $COVERITY_STREAM
        --build make --jobs=32 all
    # Next run the incremental analysis using cov-run-desktop
    # Here we set some useful options like:
    #   analyze-captured-source: Use the source code captured in the previous step
    #   ignore-uncapturable-inputs: Proceed with the analysis even if some source files could not be captured
    #   reference-snapshot: Compare to the LATEST full analysis baseline run
    #   present-in-reference: Only report findings that were NOT (False) present in the baseline - in other words,
    #                         only report newly introduced findings
    #   exit1-if-defects: Do not (False) fail with exit code if new issues found - we don't want to "break" the
    #                     build, we will provide feedback through the merge request comments
    #
    # The output will be saved to a file that we will use in the next step, to provide feedback to our developer.
    #
    - >
        cov-run-desktop
        --dir idir
        --host $COVERITY_HOST
        --port 8448
        --ssl
        --on-new-cert trust
        --user ristow
        --stream $COVERITY_STREAM
        --ignore-uncapturable-inputs true
        --reference-snapshot latest
        --present-in-reference false
        --exit1-if-defects false
        --text-output coverity-output-oneline.txt
        --text-output-style oneline
        $CHANGED_FILES        

    # Read the data from our desktop analysis - this file contains the results found by the analysis
    - export COMMENT_BODY=`cat coverity-output-oneline.txt`
    # This is a small workaround to handle the fact that YML does not like files with colons in them.
    - "export PRIVATE_TOKEN=PRIVATE-TOKEN:"
    # Here is the magic - invoke curl to post an API call to GitLab with:
    #   - The access token we defined earlier
    #   - The API endpoint, provided by the internal GitLab mechanisms
    #   - The Project ID, provided by the internal GitLab mechanisms
    #   - The Merge Request ID within the scope of the Project, provided by the internal GitLab mechanisms
    #   - URL-encoded contents of the analysis as the comment body
    - >
        curl
        --request POST
        --header "$PRIVATE_TOKEN $CI_MERGE_REQUEST_ACCESS_TOKEN"
        "$CI_API_V4_URL"/projects/"$CI_MERGE_REQUEST_PROJECT_ID"/merge_requests/"$CI_MERGE_REQUEST_IID"/notes
        --data-urlencode "body=$COMMENT_BODY"
        
